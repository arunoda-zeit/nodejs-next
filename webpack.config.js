const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
// var DashboardPlugin = require('webpack-dashboard/plugin')

const paths = {
  src: path.join(__dirname, 'src'),
  dist: path.join(__dirname, 'dist')
}

module.exports = {
  entry: paths.src + '/index.js',
  mode: 'production',
  output: {
    filename: 'bundle.js',
    path: paths.dist,
  },
  devtool: 'source-maps',
  resolve: {
    extensions: ['.js', '.json']
  },
  module: {
    rules: [
      {
        test: /.js?$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ],
  },
  plugins: [new UglifyJSPlugin()]
}
